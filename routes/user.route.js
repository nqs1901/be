const express = require('express');

const router = express.Router();

const validate = require('../middlewares/validate.mdw');

const schema = require('../schemas/user.json');

const userModel = require('../models/user.model');

const bcrypt = require('bcryptjs');



router.route('/:id')
.get(async function (req, res) {
    const id = +req.params.id || 0;
    const user = await userModel.findById(id);
  
    if (user === null) {
      return res.status(204).end();
    }
    res.json(user);
})
.delete(async function (req, res) {
    const id = +req.params.id || 0;

    if (id === 0) {
      return res.json({
        message: 'NO USER DELETED.'
      })
    }

    const affected_rows = await userModel.del(id);
    if (affected_rows === 0) {
      return res.json({
        message: 'NO USER DELETED.'
      });
    }

    res.json({
        message: 'USER DELETED.'
      });
})


router.route('/')
.get(async function (req, res) {
    res.status(200).json(await userModel.findAll());
})
.post(validate(schema), async function (req, res) {
    const user = req.body;

    const username = await userModel.findByUserName(req.body.username);
    if (username !== null) {
      return res.json({
        message: 'USERNAME EXISTED.'
      });
    }

    user.password = bcrypt.hashSync(user.password, 10);

    const result = await userModel.add(user);
    delete user.password;
  
    user.id = result[0];
    res.status(201).json(user);
})
.patch(async function (req, res) {
    const user = req.body;

    const id = user.id;
    delete user.id;
  
    const affected_rows = await userModel.patch(id, user);
    if (affected_rows === 0) {
      return res.status(304).end();
    }
  
    res.json(user);
})

module.exports = router;