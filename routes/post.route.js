const express = require('express');
const router = express.Router();
const postModel = require('../models/post.model');
const savePostSchema = require('../schemas/savePost.json');
const validate = require('../middlewares/validate.mdw');
const addressModel = require('../models/address.model');
const contactModel = require('../models/contact.model');

router.get('/', async function (req, res) {  
    const rows = await postModel.findAll();
    res.json(rows);
})

router.get('/:id', async function (req, res) {
    const id = +req.params.id || 0;  
    const post = await postModel.findById(id);
    if (post === null){
        return res.status(204).end();
    }
    res.json(post); 
})

router.post('/', validate(savePostSchema), async function (req, res) {
    const data = req.body;

    const contact = {
        fullname: data.fullname,
        phone: data.phone,
        facebook: data.facebook ? data.facebook : null,
        zalo: data.zalo ? data.zalo : null
    }

    const address = {
        apartment_number: data.apartment_number,
        street: data.street,
        ward: data.ward,
        district: data.district,
        city: data.city
    }

    const resultSaveContact = await contactModel.add(contact);
    const resultSaveAddress = await addressModel.add(address);

    const post = {
        title: data.title,
        price: +data.price,
        status: 1, // TODO: In this flow is hard code to release list post in homepage
        area: +data.area,
        address_id: resultSaveAddress[0],
        contact_id: resultSaveContact[0],
        description: data.description ? data.description : null,
        account_id: +data.account_id,
        posting_date: new Date(),
        image: data.image ? JSON.stringify(data.image) : null
    }
    const resultSavePost = await postModel.add(post);
    post.post_id = resultSavePost[0];

    res.status(201).json(post);
})

module.exports = router;