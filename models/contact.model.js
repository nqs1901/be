
const db = require('../utils/db');

module.exports = {
    add(contact) {
        return db('contact').insert(contact);
    },
};