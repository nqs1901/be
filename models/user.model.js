const db = require('../utils/db');

module.exports = {
    findAll() {
      return db('account');
    },
  
    async findById(id) {
      const rows = await db('account').where('account_id', id);
      if (rows.length === 0) {
        return null;
      }
  
      return rows[0];
    },

    async findByUserName(username) {
        const rows = await db('account').where('username', username);
        if (rows.length === 0) {
          return null;
        }
    
        return rows[0];
      },
  
    add(film) {
      return db('account').insert(film);
    },
  
    del(id) {
      return db('account')
        .where('account_id', id)
        .del();
    },
  
    patch(id, filmWithoutId) {
      return db('account')
        .where('account_id', id)
        .update(filmWithoutId);
    },

    async isValidRefreshToken(id,refreshToken){
      const result= await db('account').where({account_id:id}).andWhere({rfToken:refreshToken});
      if(result.length === 0){
          return null;
      }
      return  result[0];
  }
  };