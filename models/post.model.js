
const db = require('../utils/db');
const status = require('../constant/status.constant');

module.exports = {
    async findAll(){
        let rows = await db('post')
        .join('address', 'post.address_id', '=', 'address.address_id')
        .join('contact', 'post.contact_id', '=', 'contact.contact_id')
        .where('post.status', status.IS_APPROVED)
        .andWhere('post.is_deleted', 0);

        rows = rows.map(value => {
            const image = JSON.parse(value.image);
            const newValue = { ...value, image }
            return newValue;
        })
        return rows;
    },

    async findById(id){
        let rows = await db('post')
        .join('address', 'post.address_id', '=', 'address.address_id')
        .join('contact', 'post.contact_id', '=', 'contact.contact_id')
        .where('post.post_id', id)
        .andWhere('post.status', status.IS_APPROVED)
        .andWhere('post.is_deleted', 0);

        if (rows.length === 0){
            return null;
        }
        const result = { ...rows[0], image: JSON.parse(rows[0].image) }
        return result;
    },

    add(post) {
        return db('post').insert(post);
    },
};