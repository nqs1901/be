
const db = require('../utils/db');

module.exports = {
    add(address) {
        return db('address').insert(address);
    },
};