/**
 * @swagger
 * /api/posts:
 *   get:
 *     summary: Get list post
 *     tags: [Posts]
 *     responses:
 *       200:
 *         description: list post is approved
 */


/**
 * @swagger
 * /api/posts/{id}:
 *   get:
 *     summary: Get the post by id
 *     tags: [Posts]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The post id
 *     responses:
 *       200:
 *         description: The user description by id
 *       204:
 *         description: The post was not found
 */



/**
 * @swagger
 * components:
 *   schemas:
 *     Post:
 *       type: object
 *       required:
 *         - apartment_number
 *         - street
 *         - ward
 *         - district
 *         - city
 *         - fullname
 *         - phone
 *         - title
 *         - price
 *         - area
 *         - account_id
 *       properties:
 *         apartment_number:
 *           type: string
 *           description: apartment_number
 *         street:
 *           type: string
 *           description: street
 *         ward:
 *           type: string
 *           description: ward
 *         district:
 *           type: string
 *           description: district
 *         city:
 *           type: string
 *           description: city
 *         fullname:
 *           type: string
 *           description: fullname
 *         phone:
 *           type: string
 *           description: phone
 *         facebook:
 *           type: string
 *           description: facebook
 *         zalo:
 *           type: string
 *           description: zalo
 *         title:
 *           type: string
 *           description: title
 *         price:
 *           type: number
 *           description: price
 *         area:
 *           type: number
 *           description: area
 *         description:
 *           type: string
 *           description: description
 *         account_id:
 *           type: integer
 *           description: account_id
 *         image:
 *           type: array
 *           description: image
 *       example:
 *         apartment_number: "444"
 *         street: "Hoàng Hoa Thám"
 *         ward: "10"
 *         district: "11"
 *         city: "Hồ Chí Minh"
 *         fullname: "Nguyễn Văn B"
 *         phone: "0909723787"
 *         facebook: "facebook.com/nguyenvanb"
 *         zalo: "0909723787"
 *         title: "cho thuê nhà trọ giá rẻ"
 *         price: 3000000
 *         area: 19
 *         description: "nhà trọ đầy đủ tiện nghi"
 *         account_id: 1
 *         image: [{"path": "https://sa.tinhte.vn/2016/08/3848503_camera.tinhte.vn-14.jpg"}, {"path": "https://sa.tinhte.vn/2016/08/3848503_camera.tinhte.vn-14.jpg"}]
 */


/**
 * @swagger
 * /api/posts:
 *   post:
 *     summary: Create a new post
 *     tags: [Posts]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Post'
 *     responses:
 *       201:
 *         description: The post is created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Post'
 *       500:
 *         description: some thing broke
 *       400:
 *         description: bab request
 */