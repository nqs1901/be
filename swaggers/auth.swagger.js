// Schemas

/**
 * @swagger
 * components:
 *   schemas:
 *     Auth:
 *       type: object
 *       required:
 *         - username
 *         - password
 *       properties:
 *         username:
 *           type: string
 *           description: username
 *         password:
 *           type: string
 *           description: password
 *       example:
 *         username: d5fE_asz
 *         password: 123457
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     Payload:
 *       type: object
 *       properties:
 *         authenticated:
 *           type: boolean
 *           description: authenticated
 *         accessToken:
 *           type: string
 *           description: accessToken
 *       example:
 *         authenticated: true
 *         accessToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjksInVzZXJOYW1lIjoicHZxdWkxMTkwIiwiaWF0IjoxNjI4NDQ1NDMzLCJleHAiOjE2Mjg1MDU0MzN9.1LuwJyHT_hYS9XAEpvJgF-SU_ipHxCW5X9OdjfQwmwc
 */


/**
 * @swagger
 * /api/auth:
 *   post:
 *     summary: Auth
 *     tags: [Auth]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Auth'
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Payload'
 *       401:
 *         description: 'authenticated: false'
 * 
 */


