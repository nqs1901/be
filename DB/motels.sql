-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 11, 2021 lúc 09:51 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `motels`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `account`
--

CREATE TABLE `account` (
  `account_id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(11) COLLATE utf32_unicode_ci NOT NULL,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `account`
--

INSERT INTO `account` (`account_id`, `username`, `password`, `phone`, `email`, `role`) VALUES
(1, 'usertest1', 'user', '0909223112', NULL, 1),
(2, 'uerserTest', 'user', '0987665223', 'user@gmail.com', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `address`
--

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL,
  `apartment_number` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ward` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `district` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `address`
--

INSERT INTO `address` (`address_id`, `apartment_number`, `street`, `ward`, `district`, `city`) VALUES
(1, '422', 'Hoàng Văn Thụ', '13', '10', 'Hồ Chí Minh'),
(2, '401', 'Nguyễn Cư Trinh', '5', '5', 'Hồ Chí Minh'),
(3, '887', 'Nguyễn Thị Minh Khai', '10', '1', 'Hồ Chi Minh'),
(4, '456', 'CMT8', '13', '10', 'Hồ Chí Minh'),
(5, '456', 'CMT8', '13', '10', 'Hồ Chí Minh'),
(6, '456', 'CMT8', '13', '10', 'Hồ Chí Minh'),
(7, '456', 'CMT8', '13', '10', 'Hồ Chí Minh'),
(8, '456', 'CMT8', '13', '10', 'Hồ Chí Minh'),
(9, '456', 'CMT8', '13', '10', 'Hồ Chí Minh'),
(10, '456', 'CMT8', '13', '10', 'Hồ Chí Minh'),
(11, '456', 'CMT8', '13', '10', 'Hồ Chí Minh');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `fullname` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `zalo` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `contact`
--

INSERT INTO `contact` (`contact_id`, `fullname`, `phone`, `facebook`, `zalo`) VALUES
(1, 'Nguyễn Văn A', '0909223112', NULL, '0909887224'),
(2, 'Đinh Công Thành', '0876223114', 'https://www.facebook.com/dinhcongthanh98', '0909723112'),
(3, 'khoa', '0909998765', 'facebook.com/nguyenvana', '09098876232'),
(4, 'khoa', '0909998765', NULL, NULL),
(5, 'khoa', '0909998765', 'facebook.com/nguyenvana', '09098876232'),
(6, 'khoa', '0909998765', 'facebook.com/nguyenvana', '09098876232'),
(7, 'khoa', '0909998765', 'facebook.com/nguyenvana', '09098876232'),
(8, 'khoa', '0909998765', 'facebook.com/nguyenvana', '09098876232'),
(9, 'khoa', '0909998765', 'facebook.com/nguyenvana', '09098876232'),
(10, 'khoa', '0909998765', 'facebook.com/nguyenvana', '09098876232');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `feedback`
--

CREATE TABLE `feedback` (
  `feedback_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `feedback`
--

INSERT INTO `feedback` (`feedback_id`, `post_id`, `account_id`, `content`, `created_at`) VALUES
(1, 1, 1, 'Nhà đẹp quá', '2021-08-07 22:37:43'),
(2, 1, 1, 'haha', '2021-08-07 22:37:43'),
(3, 2, 2, 'Trước đó, theo báo cáo của Sở Y tế Đồng Nai, trong 3 đợt đầu tiên toàn tỉnh tiêm được hơn 77.000 liều vắc xin, chỉ còn khoảng 2.100 liều chuẩn bị tiêm cho doanh nghiệp Nhật Bản. Riêng trong đợt 4 (từ ngày 29-7 đến hết ngày 5-8), các cơ sở đã tiêm chủng đư', '2021-08-07 22:38:40'),
(4, 2, 1, 'Nhà này hơi chật', '2021-08-07 22:49:34'),
(5, 3, 1, 'Ca mới phân bố tại 6 quận huyện, gồm: Đông Anh, Thanh Trì, Hoàng Mai, Sóc Sơn, Thường Tín, Ba Đình.', '2021-08-07 22:50:17'),
(6, 3, 2, 'Buổi sáng, Hà Nội ghi nhận 18 ca nghi Covid-19. Trong đó, 15 người liên quan chùm ho sốt cộng đồng và những người liên quan, hai người liên quan chùm nhà thuốc Láng Hạ, một người liên quan Bắc Giang.', '2021-08-07 22:50:17'),
(7, 4, 1, 'Buổi sáng, Hà Nội ghi nhận 18 ca nghi Covid-19. Trong đó, 15 người liên quan chùm ho sốt cộng đồng và những người liên quan, hai người liên quan chùm nhà thuốc Láng Hạ, một người liên quan Bắc Giang.', '2021-08-07 22:51:12'),
(8, 4, 2, 'Trong chùm ca bệnh ho sốt và những người liên quan, 4 người là công nhân xây dựng trong Bệnh viện Đa khoa Hà Đông, đều là nam giới tuổi lần lượt 32, 37, 46, 23 tuổi. Những người này sống tại lán trong công trình. Đêm 5/8, sau khi ghi nhận 4 công nhân làm ', '2021-08-07 22:51:12'),
(9, 5, 1, 'Trong chùm ca bệnh ho sốt và những người liên quan, 4 người là công nhân xây dựng trong Bệnh viện Đa khoa Hà Đông, đều là nam giới tuổi lần lượt 32, 37, 46, 23 tuổi. Những người này sống tại lán trong công trình. Đêm 5/8, sau khi ghi nhận 4 công nhân làm ', '2021-08-07 22:51:12'),
(10, 5, 2, 'Các ca tại 8 quận huyện, gồm: Hà Đông, Đông Anh, Đống Đa, Thanh Trì, Thường Tín, Hai Bà Trưng, Long Biên, Hoàng Mai.', '2021-08-07 22:51:12');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post`
--

CREATE TABLE `post` (
  `post_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `area` float NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `address_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `description` varchar(800) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `posting_date` datetime DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `image` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `post`
--

INSERT INTO `post` (`post_id`, `title`, `price`, `area`, `status`, `address_id`, `contact_id`, `description`, `posting_date`, `expiration_date`, `account_id`, `is_deleted`, `image`) VALUES
(1, 'Cho thuê phòng giờ giấc tự do, tiện nghi 156/20 đường số 23 (Dương Quảng Hàm)', 3000000, 18, 0, 2, 1, NULL, '2021-08-17 22:32:39', NULL, 1, 0, '[\r\n  {\"path\": \"https://www.google.com/imgres?imgurl=https%3A%2F%2Fi.pinimg.com%2Foriginals%2F70%2F5c%2F24%2F705c248800445e4d2c6e9aec772cb7c6.jpg&imgrefurl=https%3A%2F%2Fwww.pinterest.com%2Fpin%2F860891285004891979%2F&tbnid=LyLdreMwq7-7AM&vet=12ahUKEwjB5eT7nZ_yAhUKIrcAHXCpAIMQMygBegUIARCrAQ..i&docid=d1XWof3AqPf1wM&w=1200&h=800&q=hinh%20anh%20nha&ved=2ahUKEwjB5eT7nZ_yAhUKIrcAHXCpAIMQMygBegUIARCrAQ\"},\r\n  {\"path\": \"https://www.google.com/imgres?imgurl=https%3A%2F%2Fnhadepsang.com.vn%2Fimages%2F2017%2F08%2F20-hinh-anh-noi-bat-cho-mau-nha-dep-1-tang-o-nong-thon-2.jpg&imgrefurl=https%3A%2F%2Fnhadepsang.com.vn%2Fmau-nha-dep%2F2017%2F08%2Fhinh-anh-noi-bat-cho-mau-nha-dep-1-tang-o-nong-thon%2F&tbnid=j--Y46dsQMHxoM&vet=12ahUKEwjB5eT7nZ_yAhUKIrcAHXCpAIMQMygHegUIARC3AQ..i&docid=l98S8Brl7dBzGM&w=700&h=498&q=hinh%20anh%20nha&ved=2ahUKEwjB5eT7nZ_yAhUKIrcAHXCpAIMQMygHegUIARC3AQ\"}\r\n]'),
(2, 'PHÒNG NGAY NGÃ TƯ HÀNG XANH giá mềm mùa dịch', 2500000, 15, 1, 1, 2, 'Trong ngày thi đấu 7-8, đoàn Philippines đã giành thêm 1 HCB nhờ công của võ sỹ quyền anh hạng ruồi Carlo Paalam. Philippines trở thành đoàn thể thao Đông Nam Á số 1 tại Olympic 2020 khi giành được 1 HCV, 2HCB và 1 HCĐ.', '2021-08-12 22:32:45', NULL, 1, 0, '[\r\n  {\"path\": \"1https://sa.tinhte.vn/2016/08/3848503_camera.tinhte.vn-14.jpg\"},\r\n  {\"path\": \"http://vuanhiepanh.com/uploads/news/ky-thuat-nhiepanh/bo-cuc-nhiep-anh/quy-tac-bo-cuc-tao-buc-anh/bo-cuc.jpg\"}\r\n]'),
(3, 'hỗ trợ giá mùa dịch - ĐẬP HỘP phòng mới tinh', 5000000, 25, 1, 2, 1, 'Chủ tịch UBND tỉnh Đồng Nai giao Sở Y tế khẩn trương chủ trì, phối hợp các cơ quan, đơn vị, địa phương rà soát, huy động cơ sở y tế ngoài công lập đủ điều kiện tiêm chủng; lực lượng quân dân y, công lập, dân lập, kể cả cán bộ y tế đã về hưu tham gia thực ', '2021-08-19 22:32:49', NULL, 1, 0, '[\r\n  {\"path\": \"https://sa.tinhte.vn/2016/08/3848503_camera.tinhte.vn-14.jpg\"},\r\n  {\"path\": \"http://vuanhiepanh.com/uploads/news/ky-thuat-nhiepanh/bo-cuc-nhiep-anh/quy-tac-bo-cuc-tao-buc-anh/bo-cuc.jpg\"}\r\n]'),
(4, 'Phòng trọ 128/5 Nguyễn Xí, Phường 13, Quận Bình Thạnh có hổ trợ giá mùa dịch', 300000, 16, 1, 2, 2, 'Trước đó, theo báo cáo của Sở Y tế Đồng Nai, trong 3 đợt đầu tiên toàn tỉnh tiêm được hơn 77.000 liều vắc xin, chỉ còn khoảng 2.100 liều chuẩn bị tiêm cho doanh nghiệp Nhật Bản. Riêng trong đợt 4 (từ ngày 29-7 đến hết ngày 5-8), các cơ sở đã tiêm chủng đư', '2021-08-15 22:32:53', NULL, 2, 0, '[\r\n  {\"path\": \"https://sa.tinhte.vn/2016/08/3848503_camera.tinhte.vn-14.jpg\"},\r\n  {\"path\": \"http://vuanhiepanh.com/uploads/news/ky-thuat-nhiepanh/bo-cuc-nhiep-anh/quy-tac-bo-cuc-tao-buc-anh/bo-cuc.jpg\"}\r\n]'),
(5, 'Cho thuê phòng ở 277/38 Đường Xô Viết Nghệ Tĩnh, Bình Thạnh, ngay Hàng Xanh', 3000000, 22, 1, 1, 1, 'UBND tỉnh Đồng Nai vừa có văn bản gửi Bộ Y tế đề nghị xem xét hỗ trợ, chi viện cho tỉnh Đồng Nai 40 bác sĩ, 60 điều dưỡng, kỹ thuật viên và các nhân viên y tế khác để tham gia điều trị, chăm sóc bệnh nhân COVID-19; điều tra, truy vết, lấy mẫu bệnh phẩm xé', '2021-08-18 22:32:58', NULL, 2, 0, '[\r\n  {\"path\": \"https://sa.tinhte.vn/2016/08/3848503_camera.tinhte.vn-14.jpg\"},\r\n  {\"path\": \"http://vuanhiepanh.com/uploads/news/ky-thuat-nhiepanh/bo-cuc-nhiep-anh/quy-tac-bo-cuc-tao-buc-anh/bo-cuc.jpg\"}\r\n]'),
(6, 'Được nuôi thú cưng - Phòng mới sạch sẽ sát Đh sư phạm', 250000, 20, 1, 1, 1, 'Như vậy, số ca dương tính nCoV trong vòng 24 giờ, từ 18h ngày 6/8 đến 18h ngày 7/8 tại Hà Nội là 82 ca. Tổng số ca mắc tại Hà Nội trong đợt dịch thứ 4, từ ngày 27/4 đến nay là 1.727 ca, trong đó số ca mắc ghi nhận ngoài cộng đồng là 1.040 ca, số mắc là đối tượng cách ly: 687 ca.', NULL, NULL, 1, 0, '[\r\n  {\"path\": \"https://sa.tinhte.vn/2016/08/3848503_camera.tinhte.vn-14.jpg\"},\r\n  {\"path\": \"http://vuanhiepanh.com/uploads/news/ky-thuat-nhiepanh/bo-cuc-nhiep-anh/quy-tac-bo-cuc-tao-buc-anh/bo-cuc.jpg\"}\r\n]'),
(9, 'Nhà trọ đẹp', 3000000, 18, 1, 10, 9, 'Hello hello house', NULL, NULL, 1, 0, '[\r\n  {\"path\": \"https://sa.tinhte.vn/2016/08/3848503_camera.tinhte.vn-14.jpg\"},\r\n  {\"path\": \"http://vuanhiepanh.com/uploads/news/ky-thuat-nhiepanh/bo-cuc-nhiep-anh/quy-tac-bo-cuc-tao-buc-anh/bo-cuc.jpg\"}\r\n]'),
(10, 'Nhà trọ đẹp', 9000000, 13, 0, 11, 10, 'Hello hello my house', NULL, NULL, 1, 0, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`account_id`);

--
-- Chỉ mục cho bảng `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`);

--
-- Chỉ mục cho bảng `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Chỉ mục cho bảng `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`feedback_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `feedback_ibfk_3` (`account_id`);

--
-- Chỉ mục cho bảng `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `post_ibfk_1` (`account_id`),
  ADD KEY `post_ibfk_2` (`address_id`),
  ADD KEY `post_ibfk_3` (`contact_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `account`
--
ALTER TABLE `account`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `feedback`
--
ALTER TABLE `feedback`
  MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `post`
--
ALTER TABLE `post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`post_id`),
  ADD CONSTRAINT `feedback_ibfk_3` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`);

--
-- Các ràng buộc cho bảng `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`),
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `address` (`address_id`),
  ADD CONSTRAINT `post_ibfk_3` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`contact_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
