process.env.NODE_ENV = 'test';

const app = require('../app');
const assert = require('assert');
const expect = require('chai').expect;
const request = require('supertest');
const supertest = require('supertest');


describe('Post tests', () => {
    var token;
    beforeEach((done) => {
        done();
    });
    describe('Login', () => {
        it('should login user gets a token', (done) => {
            request(app)
                .post('/api/auth')
                .send({
                    "username": "test",
                    "password": "123",
                })
                .end((err, res) => {
                    console.log('this runs the login part');
                    token = res.body.accessToken;
                    assert.strictEqual(res.status, 200);
                    expect(res.body.accessToken).to.not.be.undefined;
                    done();
                })
        })
    })

    describe('Posts', function () {
        it('should list ALL posts on api/posts GET', (done) => {
            request(app)
                .get('/api/posts')
                .set('authorization', token)
                .end(function (err, res) {
                    assert.strictEqual(res.status, 200);
                    expect(res.body).to.be.an('array');
                    expect(res.body[0]).to.have.property('post_id');
                    expect(res.body[0]).to.have.property('title');
                    expect(res.body[0]).to.have.property('price');
                    expect(res.body[0]).to.have.property('area');
                    expect(res.body[0]).to.have.property('address_id');
                    expect(res.body[0]).to.have.property('contact_id');
                    expect(res.body[0]).to.have.property('account_id');
                    expect(res.body[0]).to.have.property('image');
                    done();
                })
        });
        it('should list a SINGLE post on api/posts/<id> GET', (done) => {
            let id = 2;
            request(app)
                .get('/api/posts/' + id)
                .set('authorization', token)
                .end((err, res) => {
                    assert.strictEqual(res.status, 200);
                    expect(res.body).to.have.property('post_id').eql(id);
                    expect(res.body).to.have.property('title');
                    expect(res.body).to.have.property('price');
                    expect(res.body).to.have.property('area');
                    expect(res.body).to.have.property('address_id');
                    expect(res.body).to.have.property('contact_id');
                    expect(res.body).to.have.property('account_id');
                    expect(res.body).to.have.property('image');
                    done();
                })
        });
        it('should add a SINGLE post on api/posts POST', (done) => {
            const post = {
                apartment_number: "444",
                street: "Hoàng Hoa Thám",
                ward: "10",
                district: "11",
                fullname: "Nguyễn Văn B",
                phone: "0909723787",
                facebook: "facebook.com/nguyenvanb",
                zalo: "0909723787",
                city: "Hồ Chí Minh",
                title: "cho thuê nhà trọ giá rẻ",
                price: 3000000,
                area: 19,
                description: "nhà trọ đầy đủ tiện nghi",
                account_id: 1,
                image: [
                    {
                        path: "https://sa.tinhte.vn/2016/08/3848503_camera.tinhte.vn-14.jpg"
                    },
                    {
                        path: "https://sa.tinhte.vn/2016/08/3848503_camera.tinhte.vn-14.jpg"
                    }
                ]
            }
            request(app)
                .post('/api/posts')
                .send(post)
                .set('authorization', token)
                .end((err, res) => {
                    assert.strictEqual(res.status, 201);  
                    expect(typeof res.body).to.equal('object');          
                    expect(res.body).to.have.property('title');
                    expect(res.body).to.have.property('price');
                    expect(res.body).to.have.property('area');
                    expect(res.body).to.have.property('image');
                    done();
                })
        });
    })
    console.log(token)

    // it('should update a SINGLE post on api/posts/<id> PUT');
    // it('should delete a SINGLE post on api/posts/<id> DELETE');
});
